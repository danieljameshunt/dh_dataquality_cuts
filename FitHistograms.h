#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TVector3.h"
#include "TApplication.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TImage.h"

TFile* KrFile;
TFile* RnFile;
TFile* UnphyFile;
TFile* WSFile;
TFile* DDFile;

void FitHistograms();
void S2w(TFile* File, string Name, int NBins);
void TBA(TFile* File, string Name);

void Overlay(TH2D* hist, TF1 Max, TF1 Min, TF1 Mean, char* dir, char* file);
void FitLines(TF1& Max, TF1& Min, TF1& Mean, TGraph gMax, TGraph gMin, TGraph gMean, char* dir);
void FitSlices(TH2D* hist, int N, int n, double lower, double upper, TGraph& Mean, TGraph& Lower, TGraph& Upper, char* name, bool square, int rebin);

void UnphyMC(TH1D* hist, TF1 max, TF1 min, char* dir, char* file, char* title);