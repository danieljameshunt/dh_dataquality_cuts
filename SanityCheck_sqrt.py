import matplotlib.pyplot as plt 
import numpy as np

rng = np.random.default_rng()

A = 2754
B = 350841

def sqrtlin(x):
    return np.sqrt(A*x + B)


# Plot square root relationship with gaussian smearing

driftTime = 900*rng.random(5000) + 50

widths = sqrtlin(driftTime)

widths = [width+(150*rng.normal()) for width in widths]

print("Calculated S2Ws, plotting...")

fig1, ax1 = plt.subplots()
ax1.plot(driftTime, widths, '.')
fig1.savefig("SanityS2W.png")

# Retrieve expected gradient with assumed constant

assumed_constant = 600

predicted_grad = widths

for i in range(len(driftTime)):
    width = widths[i]
    DT = driftTime[i]
    # w^2 = ax + b so a = w^2 - b / x
    predicted_grad[i] = (width*width - assumed_constant)/DT

#predicted_grad = [widths[i]*widths[i] - assumed_constant / driftTime[i] for i in range(len(driftTime))]

print("Predicted gradients, plotting...")

fig2, ax2 = plt.subplots()
ax2.plot(driftTime, predicted_grad, '.')
fig2.savefig("SanityS2Grad.png")

# Retrieve expected constant with assumed gradient

assumed_gradient = 2750

predicted_constant = widths

for i in range(len(widths)):
    width = widths[i]
    DT = driftTime[i]
    # w^2 = ax + b so b = w^2 - ax
    predicted_constant[i] = (width*width) - (assumed_gradient*driftTime)

print("Predicted constants, plotting...")

fig3, ax3 = plt.subplots()
ax3.plot(driftTime, predicted_constant, '.')
fig3.savefig("SanityS2Const.png")

print("Done!")


# Try to obtain gradient with assumed constant

# Try to obtain constant with assumed gradient

# How close is this to the original value?