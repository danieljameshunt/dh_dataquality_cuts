#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

int NBinsS2W = 100;
int NBinsTBA = 100;

double minS2 = 2.5;
double minS1 = 2.5;
double maxS2 = 8;
double maxS2_ROI = 5;

double maxS1 = 1000;
double maxS1_ROI = 200;

char Cuts[400];

TFile* KrData;
TFile* RnData;
TFile* SR1Data;
TFile* DDData;
TFile* TritiumData;

void stats_TBA_Test();
void stats_S2W_Test();
void stats_TBA_Gif();
void S2W_Gif();
void TBA_Gif();
void Efficiency_Rn();
void Efficiency_All();
void Thresholds();