#ifndef CutsDH_DataQuality_Cuts_H
#define CutsDH_DataQuality_Cuts_H

#include "EventBase.h"
#include "DH_DataQuality_CutsEvent.h"

class CutsDH_DataQuality_Cuts {

public:
    CutsDH_DataQuality_Cuts(DH_DataQuality_CutsEvent* m_evt);
    ~CutsDH_DataQuality_Cuts();
    bool DH_DataQuality_CutsCutsOK();
    bool DH_DataQuality_CutsNumPulsesGT10();
    bool FiducialVolume(double r_cm, double drift_us, double maxDrift_us);
    bool SkinVeto(int S1start_ns);
    bool ODVeto(int S1start_ns);
    bool s2stinger();

private:
    EventBase* m_event;
    DH_DataQuality_CutsEvent * m_evt;
    ConfigSvc* m_conf;
};

#endif
