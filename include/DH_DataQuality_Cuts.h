#ifndef DH_DataQuality_Cuts_H
#define DH_DataQuality_Cuts_H

#include "Analysis.h"

#include "CutsDH_DataQuality_Cuts.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

#include "DH_DataQuality_CutsEvent.h"

#include "SparseSvc.h"

class SkimSvc;

class DH_DataQuality_Cuts : public Analysis {

public:
    DH_DataQuality_Cuts();
    ~DH_DataQuality_Cuts();

    void Initialize();
    void Execute();
    void Finalize();

    void Classify();
    void RQs();
    void MaxChannel();
    void BasicCuts();
    void Cut();

    void BookS2Width();
    void BookS2W_Handscans();
    void BookTBADT();
    void BookCut();

    void BookTBA_Slices();
    void BookS2_Slices();

    void S2Width();
    void S2W_Handscans();
    void TBADT();

    void TBA_Slices();
    void S2_Slices();

    void BAGA();
    void BookBAGA();

    void BasicPlots(string Dir);
    void BookBasicPlots(string Dir);
    
    void SkewSlices();
    void BookSkewSlices();

    void BookGasCut();
    void GasCut();

    SparseSvc* S2W_RFR;
    SparseSvc* S2W_HighW;
    SparseSvc* S2W_Good;
    SparseSvc* S2W_Bad;
    SparseSvc* S2W_UDTHigh;
    SparseSvc* S2W_UDTLow;

    SparseSvc* ROI_Gas;
    SparseSvc* ROI_Hair;

    bool DD = true;
    bool Rn = true;
    bool Rn_Hotspot = true;
    bool Rn_DT = true;
    bool UDT = true;
    bool TPCExternal = true;
    bool Basic = true;
    bool Basic_ETrain = true;
    bool WIMPROI = true;

    double X = 0;
    double Y = 0;
    double Rsq = 0;
    double driftTime_us = 0;
    double ss_S1c = 0;
    double ss_S2c = 0;
    
    int S1ID = 0;
    int S2ID = 0;

    double S2w = 0;
    double TBA = 0;

    double S2TBA = 0;
    
    double S2Start_us = 0;
    double S1Start_ns = 0;

    int TriggerType = 0;

    double S2_Amp = 0;

    int S2_low[11] = {4, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000};
    int S2_high[11] = {5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000};

    int S1_low[19] = {0, 3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 250};
    int S1_high[19] = {3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 250, 300};

    int E_Max = 1000;
    int E_Step = 20;

    double Skewness = 0;

    double Energy = 0;

    double S2W_Max = 4000;

    double grad_low = 1000;
    double grad_high = 10000;
    double int_low = 0;
    double int_high = 1500;

    double S1chFrac = 0;

    // Set to false for old data where SR1CoreCuts doesn't work!
    bool SR1 = false;

protected:
    CutsDH_DataQuality_Cuts* m_cutsDH_DataQuality_Cuts;
    ConfigSvc* m_conf;
    DH_DataQuality_CutsEvent* m_evt;

};

#endif
