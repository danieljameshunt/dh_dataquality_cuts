#ifndef DH_DataQuality_CutsEVENT_H
#define DH_DataQuality_CutsEVENT_H

#include "EventBase.h"

#include <string>
#include <vector>

#include "rqlibProjectHeaders.h"

class DH_DataQuality_CutsEvent : public EventBase {

public:
  DH_DataQuality_CutsEvent(EventBase* eventBase);
  virtual ~DH_DataQuality_CutsEvent();

  TTreeReaderValue<int> nPulses;
  TTreeReaderValue<vector<float>> tpcHGPulseArea;
  TTreeReaderValue<vector<int>> areaFractionTime50_ns;

  TTreeReaderValue<int> SS_nSingleScatters;
  TTreeReaderValue<int> SS_s1PulseID;
  TTreeReaderValue<float> SS_s1Area;
  TTreeReaderValue<float> SS_s2Area;
  TTreeReaderValue<float> SS_correctedS1Area;
  TTreeReaderValue<float> SS_correctedS2Area;
  TTreeReaderValue<float> SS_driftTime_ns;
  TTreeReaderValue<float> SS_X_cm;
  TTreeReaderValue<float> SS_Y_cm;
  TTreeReaderValue<int> SS_s2PulseID;

  TTreeReaderValue<bool> SS_isDoubleS1;

  TTreeReaderValue<int> Event_Trigger;

  TTreeReaderValue<vector<string > > TPC_Classification;

  TTreeReaderValue<int> TPC_nPulses;
  TTreeReaderValue<vector<float > > TPC_pulseArea;
  TTreeReaderValue<vector<float > > TPC_pulseTBA;
  TTreeReaderValue<vector<int > > TPC_pulseStartTime_ns;
  TTreeReaderValue<vector<int > > TPC_pulseEndTime_ns;
  TTreeReaderValue<vector<float > > TPC_pulseS2XPosition_cm;
  TTreeReaderValue<vector<float > > TPC_pulseS2YPosition_cm;
  TTreeReaderValue<vector<int > > TPC_s1PulseIDs;
  TTreeReaderValue<vector<int > > TPC_s2PulseIDs;
  TTreeReaderValue<vector<int > > TPC_s2PulseIDsByArea;

  TTreeReaderValue<vector<float>> TPC_SPE;
  TTreeReaderValue<vector<float>> TPC_Amp;

  TTreeReaderValue<vector<int> > TPCHG_AFT10;
  TTreeReaderValue<vector<int> > TPCHG_AFT25;
  TTreeReaderValue<vector<int> > TPCHG_AFT75;
  TTreeReaderValue<vector<int> > TPCHG_AFT5;
  TTreeReaderValue<vector<int> > TPCHG_AFT50;
  TTreeReaderValue<vector<int> > TPCHG_AFT90;
  TTreeReaderValue<vector<int> > TPCHG_AFT95;

  TTreeReaderValue< vector< vector<float> > > TPCHG_chPulseArea;

  TTreeReaderValue<int> OD_nHGPulses;
  TTreeReaderValue<vector<float>> OD_HGPulseArea;
  TTreeReaderValue<vector<int>> OD_HGPulseStartTime_ns;
  TTreeReaderValue<int> Skin_nPulses;
  TTreeReaderValue<vector<float>> Skin_PulseArea;
  TTreeReaderValue<vector<int>> Skin_PulseStartTime_ns;

private:
};

#endif // DH_DataQuality_CutsEVENT_H
