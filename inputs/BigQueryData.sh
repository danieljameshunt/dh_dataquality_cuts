source /global/homes/d/dhunt/ALPACATest/ALPACA/setup.sh

dataarray=("6860-6891" "6940-6946" "6961-6998" "7003" "7005" "7006" "7011-7016" "7019-7026" "7032-7038" "7041" "7047-7069" "7116" "7117" "7131" "7133" "7182-7189" "7204-7246" "7247-7256" "7293-7332" "7335-7348" "7360-7404" "7411-7423" "7432-7459" "7469-7483" "7497" "7511-7561" "7562-7570" "7596-7599" "7609-7621" "7623" "7625" "7627-7629" "7631-7667" "7671-7724" "7743-7751" "7754-7755" "7757-7771" "7773-7801" "7812-7863")

touch SR1FullSet.list

for str in ${dataarray[@]};do
    echo ${str}
    sed "s|run: '6961-6998'|run: '${str}'|g" SR1Latest.yaml > SR1Temp.yaml
    queryData SR1Temp.yaml -o TempOutput.list
    cat TempOutput.list >> SR1FullSet.list
done