#!/bin/bash

# Simulate in BACCARAT

#Change seed | \
echo ${SEED}
#touch ${BACC_DIR}S1_${SEED}.mac
#chmod -R 755 ${BACC_DIR}S1_${SEED}.mac
sed "s|/Bacc/randomSeed 53|/Bacc/randomSeed $SEED|g" ${HOME}/S1Typical.mac> ${HOME}/S1_${SEED}.mac

chmod +x S1_${SEED}.mac

#source ${BACC_DIR}/setup.sh

source /cvmfs/lz.opensciencegrid.org/BACCARAT/release-6.1.1/x86_64-centos7-gcc8-opt/setup.sh

BACCARATExecutable ${HOME}/S1_${SEED}.mac
echo "Done With Macro"
rm ${HOME}/S1_${SEED}.mac
BaccRootConverter ${HOME}/S1_${SEED}.bin
echo "BaccRootConverter Done"
BaccMCTruth ${HOME}/S1_${SEED}.root
echo "BaccMCTruth Done"

# Pass through DER

source $DER_DIR/setup.sh

DER --FileTimeStamp ${TIME_STAMP} --outDir ${OUTPUT_DIR} --fileSeqNum ${SEED} --RandomNumberSeed ${SEED} --GenerateDarkCounts false S1_${SEED}_mctruth.root

echo "DER Done"

# Process Through LZap 5.1.0 (RQ analysis or handscanning?)

source /cvmfs/lz.opensciencegrid.org/LZap/latest/x86_64-centos7-gcc8-opt/setup.sh

filename=$(ls ${OUTPUT_DIR}/lz_*_*_*${SEED}_raw.root)
echo filename
export LZAP_INPUT_FILES=$filename
export LZAP_OUTPUT_FILE=${OUTPUT_DIR}/data/lzap_S1_${SEED}.root

lzap /cvmfs/lz.opensciencegrid.org/LZap/latest/x86_64-centos7-gcc8-opt/ProductionSteeringFiles/MDC3/RunLZapMDC3.py 

echo "LZap Done. Finished!"

exit
