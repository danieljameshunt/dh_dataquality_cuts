#include "CutsDH_DataQuality_Cuts.h"
#include "ConfigSvc.h"

CutsDH_DataQuality_Cuts::CutsDH_DataQuality_Cuts(DH_DataQuality_CutsEvent* DH_DataQuality_Cuts_Event)
{
    m_evt = DH_DataQuality_Cuts_Event;
}

CutsDH_DataQuality_Cuts::~CutsDH_DataQuality_Cuts()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsDH_DataQuality_Cuts::DH_DataQuality_CutsCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}

bool CutsDH_DataQuality_Cuts::DH_DataQuality_CutsNumPulsesGT10()
{

    if ((*m_evt->nPulses) > 10) {
        std::cout << "(*m_evt->nPulses)  = " << (*m_evt->nPulses) << ", returning true " << std::endl;
        return true;
    } else {
        std::cout << "(*m_evt->nPulses)  = " << (*m_evt->nPulses) << ", returning false " << std::endl;
        return false;
    }
}

// Rough Fiducial Volume Cut
// Assumes linear relation between drift and Z
// Input argument is max drift time
bool CutsDH_DataQuality_Cuts::FiducialVolume(double r_cm, double drift_us, double maxDrift_us){

  double fv_r_cm = 68.8;
  double fv_low_z_cm = 2;
  double fv_hi_z_cm = 132.6;
  double lz_height_cm = 145.6;


  double z_cm = (maxDrift_us - drift_us) * lz_height_cm / maxDrift_us;

  if (r_cm < fv_r_cm && z_cm > fv_low_z_cm && z_cm < fv_hi_z_cm){
    return true;
  }
  else {
    return false;
  }

}

bool CutsDH_DataQuality_Cuts::SkinVeto(int S1start_ns){

  float skinThreshold = 2.5;
  //float skinThreshold = m_conf->GetConfigVar("skinThreshold", 2.5);
  //cout << skinThreshold << endl;
  int deltaT_ns = 0;
  for (int p = 0; p < (*m_evt->Skin_nPulses); p++){
    deltaT_ns = TMath::Abs((*m_evt->Skin_PulseStartTime_ns)[p] - S1start_ns);
    //cout << deltaT_ns << " " << (*m_evt->Skin_PulseArea)[p] << endl;
    if ((float)(*m_evt->Skin_PulseArea)[p] > skinThreshold && deltaT_ns < 500000){
      return false;
    }
  }
  return true;

}

bool CutsDH_DataQuality_Cuts::ODVeto(int S1start_ns){

  float odThreshold = 4.5;
  //float odThreshold = m_conf->GetConfigVar("odThreshold", 4.5);
  int deltaT_ns = 0;
  for (int p = 0; p < (*m_evt->OD_nHGPulses); p++){
    deltaT_ns = TMath::Abs((*m_evt->OD_HGPulseStartTime_ns)[p] - S1start_ns);
    if ((float)(*m_evt->OD_HGPulseArea)[p] > odThreshold && deltaT_ns < 500000){
      return false;
    }
  }
  return true;

}

bool CutsDH_DataQuality_Cuts::s2stinger()
{
  int s1pID = (*m_evt->SS_s1PulseID);
  if ( s1pID == 0 ) return false;
  
  const auto pulseTimeGap = (*m_evt->TPC_pulseStartTime_ns)[s1pID] - (*m_evt->TPC_pulseEndTime_ns)[s1pID-1];
  if ( (*m_evt->TPC_Classification)[s1pID-1] == "SE" && pulseTimeGap < 2000 ) return true;
  if ( (*m_evt->TPC_Classification)[s1pID-1] == "S2" && pulseTimeGap < 2000 ) return true;
  return false;
  
}
