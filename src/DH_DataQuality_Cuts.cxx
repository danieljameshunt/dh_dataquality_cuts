#include "DH_DataQuality_Cuts.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsDH_DataQuality_Cuts.h"
#include "EventBase.h"
#include "SR1CoreCuts.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
DH_DataQuality_Cuts::DH_DataQuality_Cuts()
    : Analysis()
    , m_evt(new DH_DataQuality_CutsEvent(m_event))
{

    m_event->Initialize();

    // Setup logging
    logging::set_program_name("DH_DataQuality_Cuts Analysis");
    // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5

    m_cutsDH_DataQuality_Cuts = new CutsDH_DataQuality_Cuts(m_evt);

    //create a config instance, this can be used to call the config variables.
    m_conf = ConfigSvc::Instance();

    S2W_RFR = new SparseSvc();
    S2W_Bad = new SparseSvc();
    S2W_Good = new SparseSvc();
    S2W_HighW = new SparseSvc();
    S2W_UDTHigh = new SparseSvc();
    S2W_UDTLow = new SparseSvc();

    ROI_Gas = new SparseSvc();
    ROI_Hair = new SparseSvc();

}

// Destructor
DH_DataQuality_Cuts::~DH_DataQuality_Cuts()
{
    delete m_cutsDH_DataQuality_Cuts;

    delete S2W_Bad;
    delete S2W_Good;
    delete S2W_HighW;
    delete S2W_RFR;
    delete S2W_UDTHigh;
    delete S2W_UDTLow;

    delete ROI_Gas;
    delete ROI_Hair;

}

// Called before event loop
void DH_DataQuality_Cuts::Initialize()
{
    INFO("Initializing DH_DataQuality_Cuts Analysis");
    if(SR1)m_cuts->sr1()->Initialize();
    if(SR1)m_cuts->sr1()->InitializeETrainVetoCustomization(true, "fullTPC", 8.07e-6, 0.723, "ByScatters", "loglog");
    if(SR1)m_cuts->sr1()->SetHotspotExclusions(std::string(std::getenv("ALPACA_TOPDIR"))+"/modules/DH_DataQuality_Cuts/inputs/HotspotRemoval_SR1.txt");

    S2W_RFR->InitializeSparse("S2W_RFR.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    S2W_Bad->InitializeSparse("S2W_Bad.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    S2W_Good->InitializeSparse("S2W_Good.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    S2W_HighW->InitializeSparse("S2W_HighW.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    S2W_UDTLow->InitializeSparse("S2W_UDTLow.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    S2W_UDTHigh->InitializeSparse("S2W_UDTHigh.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");

    ROI_Gas->InitializeSparse("ROI_Gas.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");
    ROI_Hair->InitializeSparse("ROI_Hair.txt", "/global/cfs/cdirs/lz/data/raw/commissioning/");

    BookBasicPlots("Cuts/All");
    //BookBasicPlots("Cuts/Rn");
    //BookBasicPlots("Cuts/Rn_DT");
    //BookBasicPlots("Cuts/Rn_Hotspot");
    //BookBasicPlots("Cuts/DD");
    BookBasicPlots("Cuts/UDT");
    BookBasicPlots("Cuts/Basic");
    BookBasicPlots("Cuts/Basic_ETrain");
    BookBasicPlots("Cuts/WIMPROI");
    
    BookS2Width();

    //BookS2W_Handscans();

    //BookSkewSlices();

    //BookS2_Slices();

    BookTBADT();

    //BookTBA_Slices();

    BookBAGA();

    BookGasCut();

    m_hists->BookHist("TriggerType", 2000, 0, 2000.);

    //m_hists->BookHist("S1ID/S1ID_DarkCounts", 100, 0, 10, 1000, 0, 1);
    //m_hists->BookHist("S1ID/S1ID_DarkCounts_Log", 100, 0, 10, 1000, -8, 0);

    //m_hists->BookHist("S1ID/S1ID_DarkCounts_UDT", 100, 0, 10, 1000, 0, 1);
    //m_hists->BookHist("S1ID/S1ID_DarkCounts_Log_UDT", 100, 0, 10, 1000, -8, 0);

    BookCut();

    auto tree = std::make_tuple(
        std::make_pair("DT", &driftTime_us),
        std::make_pair("S1c", &ss_S1c),
        std::make_pair("S2c", &ss_S2c),
        std::make_pair("S2W", &S2w),
        std::make_pair("TBA", &TBA)
    );

    m_hists->BookTree("AllSS", tree);
    m_hists->BookTree("BasicCuts", tree);
    m_hists->BookTree("SR1WS", tree);

}

void DH_DataQuality_Cuts::BookCut(){

    m_hists->BookHist("DQCut/Kr_S2W/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Kr_S2W/Pass/S2WDT", 1000, 0, 1000, 1000, 0, 4000);
    m_hists->BookHist("DQCut/Kr_S2W/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_S2W/Pass/S2WDT_ROI", 1000, 0, 1000, 1000, 0, 4000);              
    m_hists->BookHist("DQCut/Kr_S2W/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_S2W/Pass/S2WDT_Basic_ROI", 1000, 0, 1000, 1000, 0, 4000); 

    m_hists->BookHist("DQCut/Kr_S2W/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Kr_S2W/Fail/S2WDT", 1000, 0, 1000, 1000, 0, 4000);
    m_hists->BookHist("DQCut/Kr_S2W/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_S2W/Fail/S2WDT_ROI", 1000, 0, 1000, 1000, 0, 4000);        
    m_hists->BookHist("DQCut/Kr_S2W/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_S2W/Fail/S2WDT_Basic_ROI", 1000, 0, 1000, 1000, 0, 4000);   

    m_hists->BookHist("DQCut/Kr_TBA/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Kr_TBA/Pass/TBADT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Kr_TBA/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_TBA/Pass/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1);        
    m_hists->BookHist("DQCut/Kr_TBA/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_TBA/Pass/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1);      

    m_hists->BookHist("DQCut/Kr_TBA/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Kr_TBA/Fail/TBADT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Kr_TBA/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_TBA/Fail/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Kr_TBA/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Kr_TBA/Fail/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1);   

    m_hists->BookHist("DQCut/Rn_TBA/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Rn_TBA/Pass/TBADT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Rn_TBA/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Rn_TBA/Pass/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1);        
    m_hists->BookHist("DQCut/Rn_TBA/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Rn_TBA/Pass/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1);      

    m_hists->BookHist("DQCut/Rn_TBA/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("DQCut/Rn_TBA/Fail/TBADT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Rn_TBA/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Rn_TBA/Fail/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("DQCut/Rn_TBA/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5);
    m_hists->BookHist("DQCut/Rn_TBA/Fail/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1);   

}

void DH_DataQuality_Cuts::Cut(){

    // Here's where the actual data quality cuts go!

    // S2 Width

    // Kr
    double p0 = 361451;
    double p1 = 2392.86;

    // Rn
    //double p0 = 343861;
    //double p1 = 2744.65;

    if(S2w > TMath::Sqrt(p0 + p1*driftTime_us)){
        // Plot S1S2
        m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S2WDT", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S2WDT_ROI", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w);        

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Kr_S2W/Pass/S2WDT_Basic_ROI", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w); 

            }

        }

    }
    else{

        // Plot S1S2
        m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S2WDT", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S2WDT_ROI", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w);        

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Kr_S2W/Fail/S2WDT_Basic_ROI", 1000, 0, 1000, 1000, 0, 4000, driftTime_us, S2w);   

            }

        }

    }

    // TBA Cut

    // Kr
    double minKr[5] = {-0.304516, -0.000772908, 6.40951e-08, -2.00941e-10, 3.00537e-13};
    double maxKr[5] = {0.170184, -0.000386112, -2.02168e-06, 2.55463e-09, -1.02824e-12};

    double minTBA = minKr[0]
        + minKr[1]*driftTime_us 
        + minKr[2]*driftTime_us*driftTime_us 
        + minKr[3]*driftTime_us*driftTime_us*driftTime_us 
        + minKr[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

    double maxTBA = maxKr[0]
        + maxKr[1]*driftTime_us 
        + maxKr[2]*driftTime_us*driftTime_us 
        + maxKr[3]*driftTime_us*driftTime_us*driftTime_us 
        + maxKr[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

    if(TBA > minTBA && TBA < maxTBA){
        // Plot S1S2
        m_hists->BookFillHist("DQCut/Kr_TBA/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Kr_TBA/Pass/TBADT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Kr_TBA/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Kr_TBA/Pass/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);        

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Kr_TBA/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Kr_TBA/Pass/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);      

            }

        }

    }
    else{

        // Plot S1S2
        m_hists->BookFillHist("DQCut/Kr_TBA/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Kr_TBA/Fail/TBADT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Kr_TBA/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Kr_TBA/Fail/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Kr_TBA/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Kr_TBA/Fail/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);   

            }       

        }

    }


    // Rn
    double minRn[5] = {-0.188081, -0.000565691, -9.80611e-07, 1.44719e-09, -6.18911e-13};
    double maxRn[5] = {0.0770965, -0.000919074, 2.69854e-07, -8.40995e-10, 6.87098e-13};

    minTBA = minRn[0]
        + minRn[1]*driftTime_us 
        + minRn[2]*driftTime_us*driftTime_us 
        + minRn[3]*driftTime_us*driftTime_us*driftTime_us 
        + minRn[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

    maxTBA = maxRn[0]
        + maxRn[1]*driftTime_us 
        + maxRn[2]*driftTime_us*driftTime_us 
        + maxRn[3]*driftTime_us*driftTime_us*driftTime_us 
        + maxRn[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

    if(TBA > minTBA && TBA < maxTBA){
        // Plot S1S2
        m_hists->BookFillHist("DQCut/Rn_TBA/Pass/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Rn_TBA/Pass/TBADT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Rn_TBA/Pass/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Rn_TBA/Pass/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);        

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Rn_TBA/Pass/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Rn_TBA/Pass/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);      

            }

        }

    }
    else{

        // Plot S1S2
        m_hists->BookFillHist("DQCut/Rn_TBA/Fail/S1S2", 1000, 0, 10000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        
        // Plot S2WDT
        m_hists->BookFillHist("DQCut/Rn_TBA/Fail/TBADT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        
        if(WIMPROI){

            // Plot S1S2
            m_hists->BookFillHist("DQCut/Rn_TBA/Fail/S1S2_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
            // Plot S2WDT
            m_hists->BookFillHist("DQCut/Rn_TBA/Fail/TBADT_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);

            if(Basic_ETrain){

                // Plot S1S2
                m_hists->BookFillHist("DQCut/Rn_TBA/Fail/S1S2_Basic_ROI", 1000, 0, 200, 1000, 2, 5, ss_S1c, TMath::Log10(ss_S2c));
        
                // Plot S2WDT
                m_hists->BookFillHist("DQCut/Rn_TBA/Fail/TBADT_Basic_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);   

            }       

        }

    }

}

// Execute() - Called once per event.
void DH_DataQuality_Cuts::Execute()
{
  if(SR1)m_cuts->sr1()->IsTPCMuon();
  if(SR1)m_cuts->sr1()->InitializeETrainVetoInEvent();
  // Check if event is Radon, DD, or Unphysical
  if((*m_evt->SS_nSingleScatters)){
    RQs();
    m_hists->BookFillHist("TriggerType", 2000, 0., 2000., (double)TriggerType);
    BasicCuts();
    Classify();
    BasicPlots("Cuts/All");
    if(Basic) BasicPlots("Cuts/Basic");
    if(Basic_ETrain) BasicPlots("Cuts/Basic_ETrain");
    //if(Rn) BasicPlots("Cuts/Rn");
    //if(Rn_Hotspot) BasicPlots("Cuts/Rn_Hotspot");
    //if(Rn_DT) BasicPlots("Cuts/Rn_DT");
    //if(DD) BasicPlots("Cuts/DD");
    if(UDT) BasicPlots("Cuts/UDT");
    if(WIMPROI)BasicPlots("Cuts/WIMPROI");
    S2Width();
    //S2W_Handscans();
    //S2_Slices();
    //MaxChannel();

    TBADT();
    //TBA_Slices();

    BAGA();

    GasCut();

    //if(Basic_ETrain)SkewSlices();

    Cut();

  }
  if(SR1)m_cuts->sr1()->DeleteInactiveProgenitors();
}

void DH_DataQuality_Cuts::GasCut(){
    if(Basic_ETrain){
    int nPulses = (*m_evt->nPulses);

    double S2Time = 0;

    if(nPulses > S2ID){
        // New bad area method - 1.5ms before S2 is used
        S2Time = (*m_evt->TPC_pulseStartTime_ns)[S2ID];
    }
    
    // Check 60us before S2
    double gasThreshold = S2Time - 60E3;
    // And 120us before S2
    double backgroundThreshold = S2Time - 120E3;

    int gasSPEs = 0;
    int BGSPEs = 0;

    for(int ID = S2ID - 1; ID > 0; ID --){
        if((*m_evt->TPC_SPE)[ID]){
            double Time = (*m_evt->TPC_pulseStartTime_ns)[ID];
            if(Time > gasThreshold)gasSPEs+=1;
            if(Time < gasThreshold && Time > backgroundThreshold)BGSPEs+=1;
            if(Time < backgroundThreshold)break;
        }
    }

    // This rate should be above average for gas events, since SPEs are more likely to reach the top of the TPC
    double gasRate = ((double)(gasSPEs - BGSPEs))/(60E-6);

    // Amplitude to width should be very different here
    double Amp = (*m_evt->TPC_Amp)[S2ID];
    double Amp_Width = TMath::Log10(Amp/S2w);

    m_hists->BookFillHist("Gas/SPER_DT", 1000, 0, 1000, 25, 0, 400000, driftTime_us, gasRate);

    m_hists->BookFillHist("Gas/SPER_AmpW", 1000, -8., 1., 25, 0., 400000., Amp_Width, gasRate);

    if(driftTime_us < 60){
        m_hists->BookFillHist("Gas/SPER_AmpW_GAS", 1000, -8, 1, 25, 0, 400000, Amp_Width, gasRate);
    }

    if(driftTime_us < 60){
        m_hists->BookFillHist("Gas/S1S2", 1000, 0, 1000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
    }

    // What does a cut here look like in S2W vs DT space?
    if(gasRate > 20E3){
        m_hists->BookFillHist("Gas/AmpW_Pass", 1000, 0, 1000, 1000, -8, 1, driftTime_us, Amp_Width);
        m_hists->BookFillHist("Gas/S2WDT_Pass", 1000, 0, 1000, 400, 0, 4000, driftTime_us, S2w);
        if(Amp_Width < -1.5 && Amp_Width > -3.5){
            m_hists->BookFillHist("Gas/S2WDT_GAS", 1000, 0, 1000, 400, 0, 4000, driftTime_us, S2w);
            m_hists->BookFillHist("Gas/TBADT_GAS", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
            m_hists->BookFillHist("Gas/S1S2_GAS", 1000, 0, 1000, 1000, 2, 6, ss_S1c, TMath::Log10(ss_S2c));
        }
        
    }
    else{
        m_hists->BookFillHist("Gas/S2WDT_Fail", 1000, 0, 1000, 400, 0, 4000, driftTime_us, S2w);
        m_hists->BookFillHist("Gas/AmpW_Fail", 1000, 0, 1000, 1000, -8, 1, driftTime_us, Amp_Width);
    }
    }
}

void DH_DataQuality_Cuts::BookGasCut(){

    m_hists->BookHist("Gas/SPER_DT", 1000, 0, 1000, 25, 0., 400000.);

    m_hists->BookHist("Gas/SPER_AmpW", 1000, -8., 1., 25, 0., 400000.);

    m_hists->BookHist("Gas/SPER_AmpW_GAS", 1000, -8., 1., 25, 0., 400000.);

    m_hists->BookHist("Gas/S1S2", 1000, 0, 1000, 1000, 2, 6);

    m_hists->BookHist("Gas/S1S2_GAS", 1000, 0, 1000, 1000, 2, 6);

}

void DH_DataQuality_Cuts::BAGA(){

    if(Basic_ETrain){

        // Define good area

        double goodArea = ss_S1c + ss_S2c;

        // Define bad area

        double badArea = 0;

        int nPulses = (*m_evt->nPulses);

        double S2Time = 0;

        if(nPulses > S2ID){
            // New bad area method - 1.5ms before S2 is used
            S2Time = (*m_evt->TPC_pulseStartTime_ns)[S2ID];
        }
        double threshold = S2Time - 1.5E6;

        for(int ID = S2ID - 1; ID > 0; ID --){
        if(ID != S2ID && ID != S1ID){
            badArea += (*m_evt->TPC_pulseArea)[ID];
        }
        double Time = (*m_evt->TPC_pulseStartTime_ns)[ID];
        if(Time < threshold)break;
        //if(badArea > 1E9 || badArea < -10)cout << badArea << endl;
        }

        double BA_Asym = (goodArea - badArea)/(goodArea + badArea);

        double BA_Ratio = badArea/goodArea;

        // Plot for S1c, S2c, GA, BA

        // Bad Area
        m_hists->BookFillHist("BAGA/BA/GA", 1000, 2, 6, 1000, 0, 6, TMath::Log10(goodArea), TMath::Log10(badArea));
        m_hists->BookFillHist("BAGA/BA/S1c", 1000, 0, 10000, 1000, 0, 6, ss_S1c, TMath::Log10(badArea));
        m_hists->BookFillHist("BAGA/BA/S1c_ROI", 1000, 0, 100, 1000, 0, 6, ss_S1c, TMath::Log10(badArea));
        m_hists->BookFillHist("BAGA/BA/S2c", 1000, 2, 6, 1000, 0, 6, TMath::Log10(ss_S2c), TMath::Log10(badArea));

        // Bad Area Ratio
        m_hists->BookFillHist("BAGA/BARatio/BA", 1000, 0, 6, 1000, -6, 2, TMath::Log10(badArea), TMath::Log10(BA_Ratio));
        m_hists->BookFillHist("BAGA/BARatio/GA", 1000, 2, 6, 1000, -6, 2, TMath::Log10(goodArea), TMath::Log10(BA_Ratio));
        m_hists->BookFillHist("BAGA/BARatio/S1c", 1000, 0, 10000, 1000, -6, 2, ss_S1c, TMath::Log10(BA_Ratio));
        m_hists->BookFillHist("BAGA/BARatio/S1c_ROI", 1000, 0, 100, 1000, -6, 2, ss_S1c, TMath::Log10(BA_Ratio));
        m_hists->BookFillHist("BAGA/BARatio/S2c", 1000, 2, 6, 1000, -6, 2, TMath::Log10(ss_S2c), TMath::Log10(BA_Ratio));

        // Bad Area Asymmetry
        m_hists->BookFillHist("BAGA/BAAsym/BA", 1000, 0, 6, 1000, -1, 1, TMath::Log10(badArea), BA_Asym);
        m_hists->BookFillHist("BAGA/BAAsym/GA", 1000, 2, 6, 1000, -1, 1, TMath::Log10(goodArea), BA_Asym);
        m_hists->BookFillHist("BAGA/BAAsym/S1c", 1000, 0, 10000, 1000, -1, 1, ss_S1c, BA_Asym);
        m_hists->BookFillHist("BAGA/BAAsym/S1c_ROI", 1000, 0, 100, 1000, -1, 1, ss_S1c, BA_Asym);
        m_hists->BookFillHist("BAGA/BAAsym/S2c", 1000, 2, 6, 1000, -1, 1, TMath::Log10(ss_S2c), BA_Asym);

    }

}

void DH_DataQuality_Cuts::BookBAGA(){
    // Bad Area
    m_hists->BookHist("BAGA/BA/GA", 1000, 2, 6, 1000, 2, 6);
    m_hists->BookHist("BAGA/BA/S1c", 1000, 0, 10000, 1000, 2, 6);
    m_hists->BookHist("BAGA/BA/S1c_ROI", 1000, 0, 100, 1000, 2, 6);
    m_hists->BookHist("BAGA/BA/S2c", 1000, 2, 6, 1000, 2, 6);

    // Bad Area Ratio
    m_hists->BookHist("BAGA/BARatio/BA", 1000, 2, 6, 1000, -6, 2);
    m_hists->BookHist("BAGA/BARatio/GA", 1000, 2, 6, 1000, -6, 2);
    m_hists->BookHist("BAGA/BARatio/S1c", 1000, 0, 10000, 1000, -6, 2);
    m_hists->BookHist("BAGA/BARatio/S1c_ROI", 1000, 0, 100, 1000, -6, 2);
    m_hists->BookHist("BAGA/BARatio/S2c", 1000, 2, 6, 1000, -6, 2);

    // Bad Area Asymmetry
    m_hists->BookHist("BAGA/BAAsym/BA", 1000, 2, 6, 1000, -1, 1);
    m_hists->BookHist("BAGA/BAAsym/GA", 1000, 2, 6, 1000, -1, 1);
    m_hists->BookHist("BAGA/BAAsym/S1c", 1000, 0, 10000, 1000, -1, 1);
    m_hists->BookHist("BAGA/BAAsym/S1c_ROI", 1000, 0, 100, 1000, -1, 1);
    m_hists->BookHist("BAGA/BAAsym/S2c", 1000, 2, 6, 1000, -1, 1);
}

void DH_DataQuality_Cuts::BookSkewSlices(){

    int MaxDT = 1000.;
    int DTStep = 100.;
    int N_DT = (int)((int)MaxDT/(int)DTStep);

    int DT_Max = DTStep;
    int DT_Min = 0;

    char plot[400];

    for(int i = 0; i < N_DT; i++){

        if(driftTime_us > DT_Min && driftTime_us < DT_Max)break;

        sprintf(plot, "S2W/DTSlices/slice_%i_%i/S2WDT", (int)DT_Min, (int)DT_Max);
        m_hists->BookFillHist(plot, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        sprintf(plot, "S2W/DTSlices/slice_%i_%i/S2WSkew", (int)DT_Min, (int)DT_Max);
        m_hists->BookFillHist(plot, 1000, -1, 1, 400, 0, S2W_Max, Skewness, S2w);    

        DT_Max += DTStep;
        DT_Min += DTStep;        

    }
}

void DH_DataQuality_Cuts::SkewSlices(){

    int MaxDT = 1000;
    int DTStep = 100;
    int N_DT = (int)(MaxDT/DTStep);

    int DT_Max = DTStep;
    int DT_Min = 0;

    for(int i = 0; i < N_DT; i++){

        if(driftTime_us > DT_Min && driftTime_us < DT_Max)break;

        DT_Max += DTStep;
        DT_Min += DTStep;        

    }

    char plot[400];

    sprintf(plot, "S2W/DTSlices/slice_%i_%i/S2WDT", (int)DT_Min, (int)DT_Max);
    m_hists->BookFillHist(plot, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

    sprintf(plot, "S2W/DTSlices/slice_%i_%i/S2WSkew", (int)DT_Min, (int)DT_Max);
    m_hists->BookFillHist(plot, 1000, -1, 1, 400, 0, S2W_Max, Skewness, S2w);    

}

void DH_DataQuality_Cuts::MaxChannel(){
    // Define max channel area here

    double S1MaxChArea = 0;

    vector<float> S1chArea;
    double A = 0;
    int NChID = ((*m_evt->TPCHG_chPulseArea)[S1ID]).size();

    // This does work but is slow
    if(S1ID > 0 && S1ID < 1000){
        if(((*m_evt->TPCHG_chPulseArea)[S1ID]).size() > 0 && ((*m_evt->TPCHG_chPulseArea)[S1ID]).size() < 500){
        try{
        S1chArea = (*m_evt->TPCHG_chPulseArea)[S1ID];
        //cout << "S1: " << S1chArea.size() << " " << S1chArea[1] << endl;
        if(S1chArea.size() > 0 && S1chArea.size() < 500)S1MaxChArea = (double)(*max_element(S1chArea.begin(), S1chArea.end()));
        }
        catch(int i){cout << "Segfault here!" << endl;}
        }
    }
    
    S1chArea.clear();

    if(ss_S1c > 0)S1chFrac = S1MaxChArea / ss_S1c;


    // Plot against S1c for low S1c

    m_hists->BookFillHist("S1ID/S1ID_DarkCounts", 100, 0, 10, 1000, 0, 1, ss_S1c, S1chFrac);
    m_hists->BookFillHist("S1ID/S1ID_DarkCounts_Log", 100, 0, 10, 1000, -8, 0, ss_S1c, TMath::Log10(S1chFrac));

    if(UDT){
        m_hists->BookFillHist("S1ID/S1ID_DarkCounts_UDT", 100, 0, 10, 1000, 0, 1, ss_S1c, S1chFrac);
        m_hists->BookFillHist("S1ID/S1ID_DarkCounts_Log_UDT", 100, 0, 10, 1000, -8, 0, ss_S1c, TMath::Log10(S1chFrac));
    }

}

void DH_DataQuality_Cuts::BookS2_Slices(){
    // Book slices of S2 width for - S1 area, S2 area, Energy (rough)

    char name[400];

    // Check S2 area against slices
    double SE = 60;

    int S2 = 0;

    for(int i = 0; i < sizeof(S2_low); i++){

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDT", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDTSQ", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S1S2", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S2WDT", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S2WDTSQ", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S1S2", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDT", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDTSQ", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S1S2", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);
        
    }

    // Check S1 area against slices

    for(int i = 0; i < 19; i++){
        
        int Low = S1_low[i];
        int High = S1_high[i];

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDT", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S1S2", Low, High);
        // m_hists->BookHist(name, 1000, 0, 300, 400, 0, S2W_Max);

        sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S2WDT", Low, High);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S2WDTSQ", Low, High);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S1S2", Low, High);
        m_hists->BookHist(name, 1000, 0, 300, 400, 0, 7);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDT", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S1S2", Low, High);
        // m_hists->BookHist(name, 1000, 0, 300, 400, 0, 7);

    }

    // Generate preliminary energy estimate, check against slices

    int N_E = (int)(E_Max/E_Step);

    for(int i = 0; i < N_E; i++){
        int E_low = i*E_Step;
        int E_high = E_low + E_Step;

        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S2WDT", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S2WDTSQ", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S1S2", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);
    }

}

void DH_DataQuality_Cuts::S2_Slices(){
    // Take slices of S2 width for - S1 area, S2 area, Energy (rough)

    // Check S2 area against slices
    double SE = 60;

    int S2 = 0;

    for(int i = 0; i < 12; i++){
        double Low = S2_low[i]*SE;
        double High = S2_high[i]*SE;
        if(ss_S2c > Low && ss_S2c < High)break;
        S2 += 1;
    }

    // Check S1 area against slices

    int S1 = 0;

    for(int i = 0; i < 19; i++){
        double Low = S1_low[i];
        double High = S1_high[i];
        if(ss_S1c > Low && ss_S1c < High)break;
        S1 += 1;
    }

    // Generate preliminary energy estimate, check against slices

    int N_E = (int)(E_Max/E_Step);

    int E_low = 0;
    int E_high = 0;
    int E = 0;
    for(int i = 0; i < N_E; i++){
        E_low = i*E_Step;
        E_high = E_low + E_Step;
        if(Energy > E_low && Energy < E_high)break;
        E += 1;
    }

    char name[400];

    // Plot S2 Slices
    if(S2 < 11){
        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDT", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDTSQ", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S2WDT", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S2WDTSQ", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            sprintf(name, "S2W/S2Slices/Basic_ETrain/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDT", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDTSQ", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

    // Plot S1 Slices
    if(S1 < 19){

        int Low = S1_low[S1];
        int High = S1_high[S1];


        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDT", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S1S2", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S2WDT", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S2WDTSQ", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            sprintf(name, "S2W/S1Slices/Basic_ETrain/%i_%i/S1S2", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDT", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDTSQ", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S1S2", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

    // Plot E Slices
    if(E < N_E){
        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S2WDT", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S2WDTSQ", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            sprintf(name, "S2W/ESlices/Basic_ETrain/%i_%i/S1S2", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDT", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDTSQ", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "S2W/ESlices/Rn/%i_%i/S1S2", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

}

void DH_DataQuality_Cuts::RQs(){

  X = *m_evt->SS_X_cm;
  Y = *m_evt->SS_Y_cm;
  Rsq = X*X + Y*Y;
  driftTime_us = *m_evt->SS_driftTime_ns/1000;
  ss_S1c = *m_evt->SS_correctedS1Area;
  ss_S2c = *m_evt->SS_correctedS2Area;

  S1ID = *m_evt->SS_s1PulseID;
  S2ID = *m_evt->SS_s2PulseID;

  S2w = (*m_evt->TPCHG_AFT75)[S2ID] - (*m_evt->TPCHG_AFT25)[S2ID];
  TBA = (*m_evt->TPC_pulseTBA)[S1ID];

  S2TBA = (*m_evt->TPC_pulseTBA)[S2ID];

  S2Start_us = (*m_evt->TPC_pulseStartTime_ns)[S2ID]/1000;
  S1Start_ns = (double)(*m_evt->TPC_pulseStartTime_ns)[S1ID];

  S2_Amp = (*m_evt->TPC_Amp)[S2ID];

  // Skewness
  double AFT10 = (*m_evt->TPCHG_AFT10)[S2ID];
  double AFT50 = (*m_evt->TPCHG_AFT50)[S2ID];
  double AFT90 = (*m_evt->TPCHG_AFT90)[S2ID];
  //double AFT5 = (*m_evt->TPCHG_AFT5)[S2ID];
  //double AFT25 = (*m_evt->TPCHG_AFT25)[S2ID];
  //double AFT75 = (*m_evt->TPCHG_AFT75)[S2ID];
  //double AFT95 = (*m_evt->TPCHG_AFT95)[S2ID];
  double HighW = AFT90 - AFT50;
  double LowW = AFT50 - AFT10;
  Skewness = (HighW - LowW)/(LowW + HighW); // -1 to 1

  double W_eV = 13.4967;
  double g1 = 0.1258;
  double g2 = 51.6;
  double g2_bot = 16.0;
  
  Energy = W_eV/1000.*((ss_S1c/g1) + (ss_S2c/g2));

  // Is trigger type causing a seg fault?
  TriggerType = (*m_evt->Event_Trigger);

}

// Finalize() - Called once after event loop.
void DH_DataQuality_Cuts::Finalize()
{
    INFO("Finalizing DH_Data_Quality_Cuts Analysis");
}

void DH_DataQuality_Cuts::BookS2W_Handscans(){

    // All events - key pathologies
    // m_hists->BookHist("S2W/Handscans/RFR/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);
	// m_hists->BookHist("S2W/Handscans/RFR/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max);
    // m_hists->BookHist("S2W/Handscans/HighW/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);
    // m_hists->BookHist("S2W/Handscans/Good/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);
    // m_hists->BookHist("S2W/Handscans/Bad/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);
    // m_hists->BookHist("S2W/Handscans/UDTHigh/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);
    // m_hists->BookHist("S2W/Handscans/UDTLow/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max);

    // BookBasicPlots("S2W/Handscans/RFR");
    // BookBasicPlots("S2W/Handscans/HighW");
    // BookBasicPlots("S2W/Handscans/Good");
    // BookBasicPlots("S2W/Handscans/Bad");
    // BookBasicPlots("S2W/Handscans/UDTLow");
    // BookBasicPlots("S2W/Handscans/UDTHigh");

    // ROI Events, basic cuts - pathologies at max and min DT
    m_hists->BookHist("S2W/Handscans/Gas/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Handscans/Hair/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max);
    BookBasicPlots("S2W/Handscans/Gas");
    BookBasicPlots("S2W/Handscans/Hair");

}

void DH_DataQuality_Cuts::S2W_Handscans(){

    // Performs handscans of particular ROIs for the S2 width vs drift time cut
    // Looking at all Rn data

    // Events in RFR accidental band - low-width S2s, continuing to UDT
    // if(S2w >500 && S2w < 700){
    //     m_hists->BookFillHist("S2W/Handscans/RFR/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
	//     m_hists->BookFillHist("S2W/Handscans/RFR/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max, TBA, S2w);
    //     BasicPlots("S2W/Handscans/RFR");
    //     S2W_RFR->SparseLogData(m_event);
    // }

    // // Events in max width Rn band - overdensity of events at maximum DT pairing with random S1s, not continuing to UDT
    // if(driftTime_us < 500 && S2w > 1750 && S2w < 2000){
    //     m_hists->BookFillHist("S2W/Handscans/HighW/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("S2W/Handscans/HighW");
    //     S2W_HighW->SparseLogData(m_event);
    // }

    // // Good Events - S2s inside S2W cut
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1400 && S2w < 1600){
    //     m_hists->BookFillHist("S2W/Handscans/Good/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("S2W/Handscans/Good");
    //     S2W_Good->SparseLogData(m_event);
    // }

    // // Bad Events - S2s smearing into higher S2Ws - MS pileup and skewed S2s
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1600 && S2w < 3400){
    //     m_hists->BookFillHist("S2W/Handscans/Bad/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("S2W/Handscans/Bad");
    //     S2W_Bad->SparseLogData(m_event);
    // }

    // // UDT High
    // if(driftTime_us > 920 && driftTime_us < 960 && S2w > 2500 && S2w < 2700){
    //     m_hists->BookFillHist("S2W/Handscans/UDTHigh/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("S2W/Handscans/UDTHigh");
    //     S2W_UDTHigh->SparseLogData(m_event);
    // }

    // // UDT Low
    // if(driftTime_us > 920 && driftTime_us < 960 && S2w > 800 && S2w < 1000){
    //     m_hists->BookFillHist("S2W/Handscans/UDTLow/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("S2W/Handscans/UDTLow");
    //     S2W_UDTLow->SparseLogData(m_event);
    // }

    // WIMP ROI, basic cuts - how do these events look?

    if(WIMPROI && Basic_ETrain){
        // Gas Events dominate window below 60us
        if(driftTime_us < 60){
            BasicPlots("S2W/Handscans/Gas");
            m_hists->BookFillHist("S2W/Handscans/Gas/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max, TBA, S2w);
            ROI_Gas->SparseLogData(m_event);
        }
        // There's a 'hair' of events at maximum drift time - what are these?
        if(driftTime_us > 945 && driftTime_us < 955){
            BasicPlots("S2W/Handscans/Hair");
            m_hists->BookFillHist("S2W/Handscans/Hair/TBAS2W", 1000, -1, 1, 400, 0, S2W_Max, TBA, S2w);
            ROI_Hair->SparseLogData(m_event);
        }
    }

}

void DH_DataQuality_Cuts::S2Width()
{    

    // Plots of S2 width vs drift time for all data types
    m_hists->BookFillHist("S2W/Plots/S2WDT_All", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    m_hists->BookFillHist("S2W/Plots/S2WDTSQ_All", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

    // Plot of S2 width speicfically in WIMP ROI (S1 S2 limits)
    if(WIMPROI)m_hists->BookFillHist("S2W/Plots/S2WDT_All_ROI", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

    // Skew S2 plots
    m_hists->BookFillHist("S2W/Plots/SkewDT_All", 1000, 0, 1000, 1000, -1, 1, driftTime_us, Skewness);
    m_hists->BookFillHist("S2W/Plots/SkewE_All", 1000, 0, 2000, 1000, -1, 1, Energy, Skewness);
    m_hists->BookFillHist("S2W/Plots/SkewS2_All", 1000, 0, 6, 1000, -1, 1, TMath::Log10(ss_S2c), Skewness);
    m_hists->BookFillHist("S2W/Plots/S2WE_All", 1000, 0, 2000, 400, 0, S2W_Max, Energy, S2w);
    m_hists->BookFillHist("S2W/Plots/S2WSkew_All", 1000, -1, 1, 400, 0, S2W_Max, Skewness, S2w);

    if(Basic){
      m_hists->BookFillHist("S2W/Plots/S2WDT_Basic", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      m_hists->BookFillHist("S2W/Plots/S2WDTSQ_Basic", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);
    }

    if(Basic_ETrain){
      m_hists->BookFillHist("S2W/Plots/S2WDT_Basic_ETrain", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      if(WIMPROI)m_hists->BookFillHist("S2W/Plots/S2WDT_Basic_ETrain_ROI", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      m_hists->BookFillHist("S2W/Plots/S2WDTSQ_Basic_ETrain", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

      m_hists->BookFillHist("S2W/Plots/Basic_ETrain_TBAS2W", 1000, -1, 1, 400, 0, S2W_Max, TBA, S2w);
    }

    if(Rn){
      m_hists->BookFillHist("S2W/Plots/S2WDT_Rn", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      m_hists->BookFillHist("S2W/Plots/S2WDTSQ_Rn", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);
    }
    if(Rn_Hotspot){
      m_hists->BookFillHist("S2W/Plots/S2WDT_Rn_Hotspot", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      m_hists->BookFillHist("S2W/Plots/S2WDTSQ_Rn_Hotspot", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);
    }
    if(DD){
      m_hists->BookFillHist("S2W/Plots/S2WDT_DD", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
      m_hists->BookFillHist("S2W/Plots/S2WDTSQ_DD", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);
    }
    if(UDT){
      m_hists->BookFillHist("S2W/Plots/S2WDT_UDT", 1000, 1000, 5000, 400, 0, S2W_Max, driftTime_us, S2w);
    }

    // Assumed from Kr distribution fit
    double assumed_gradient = 2392;
    double assumed_intercept = 601*601;

    // If we assume a y intercept, does the gradient change?
    // S2w^2 = mx + c so m = (S2w^2 - c)/x
    if(Basic_ETrain){
        double predicted_gradient = (S2w*S2w - assumed_intercept)/driftTime_us;
        m_hists->BookFillHist("S2W/Gradient/S2c", 1000, 2, 6, 1000, grad_low, grad_high, TMath::Log10(ss_S2c), predicted_gradient);
        m_hists->BookFillHist("S2W/Gradient/E", 250, 0, 2500, 1000, grad_low, grad_high, Energy, predicted_gradient);
        m_hists->BookFillHist("S2W/Gradient/DT", 1000, 0, 1000, 1000, grad_low, grad_high, driftTime_us, predicted_gradient);
    }

    // If we assume a gradient, does the y intercept change?
    // S2w^2 = mx + c so c = S2w^2 - mx
    // This is also deviance add the y intercept
    if(Basic_ETrain){
        double predicted_intercept = TMath::Sqrt(S2w*S2w - (assumed_gradient*driftTime_us));
        m_hists->BookFillHist("S2W/Intercept/S2c", 1000, 2, 6, 150, int_low, int_high, TMath::Log10(ss_S2c), predicted_intercept);
        m_hists->BookFillHist("S2W/Intercept/E", 250, 0, 2500, 150, int_low, int_high, Energy, predicted_intercept);
        m_hists->BookFillHist("S2W/Intercept/DT", 1000, 0, 1000, 150, int_low, int_high, driftTime_us, predicted_intercept);

        double p0 = 361451;
        double p1 = 2392.86;
        if(S2w > TMath::Sqrt(p0 + p1*driftTime_us)){
            m_hists->BookFillHist("S2W/Intercept/S2c_Pass", 1000, 2, 6, 150, int_low, int_high, TMath::Log10(ss_S2c), predicted_intercept);
            m_hists->BookFillHist("S2W/Intercept/E_Pass", 250, 0, 2500, 150, int_low, int_high, Energy, predicted_intercept);
            m_hists->BookFillHist("S2W/Intercept/DT_Pass", 1000, 0, 1000, 150, int_low, int_high, driftTime_us, predicted_intercept);
        }
        else{
            m_hists->BookFillHist("S2W/Intercept/S2c_Fail", 1000, 2, 6, 150, int_low, int_high, TMath::Log10(ss_S2c), predicted_intercept);
            m_hists->BookFillHist("S2W/Intercept/E_Fail", 250, 0, 2500, 150, int_low, int_high, Energy, predicted_intercept);
            m_hists->BookFillHist("S2W/Intercept/DT_Fail", 1000, 0, 1000, 150, int_low, int_high, driftTime_us, predicted_intercept);
        }

    }

}

void DH_DataQuality_Cuts::BookS2Width(){

    m_hists->BookHist("S2W/Plots/S2WDT_All", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_All", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_All_ROI", 1000, 0, 1000, 400, 0, S2W_Max);

    m_hists->BookHist("S2W/Plots/SkewE_All; Energy [MeV]; S2 Skewness", 1000, 0, 2000, 1000, -1, 1);
    m_hists->BookHist("S2W/Plots/SkewS2_All; Energy [keV]; S2 Skewness", 1000, 0, 6, 1000, -1, 1);
    m_hists->BookHist("S2W/Plots/S2WE_All; Energy [keV]; S2 AFT75 - AFT25 [ns]", 1000, 0, 2000, 400, 0, S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WE_All; Energy [keV]; S2 AFT75 - AFT25 [ns]", 1000, 0, 2000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WSkew_All; Skewness; S2 AFT75 - AFT25 [ns]", 1000, -1, 1, 400, 0, S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_Basic", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_Basic", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_Basic_ETrain", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDT_Basic_ETrain_ROI", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_Basic_ETrain", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/Basic_ETrain_TBAS2W", 1000, -1, 1, 400, 0, S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_Rn", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_Rn", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_Rn_Hotspot", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_Rn_Hotspot", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_DD", 1000, 0, 1000, 400, 0, S2W_Max);
    m_hists->BookHist("S2W/Plots/S2WDTSQ_DD", 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

    m_hists->BookHist("S2W/Plots/S2WDT_UDT", 1000, 1000, 5000, 400, 0, S2W_Max);

    m_hists->BookHist("S2W/Gradient/S2c", 1000, 0, 6, 1000, grad_low, grad_high);
    m_hists->BookHist("S2W/Gradient/E", 250, 0, 2500, 1000, grad_low, grad_high);
    m_hists->BookHist("S2W/Gradient/DT", 1000, 0, 1000, 1000, grad_low, grad_high);

    m_hists->BookHist("S2W/Intercept/S2c", 1000, 0, 6, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/E", 250, 0, 2500, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/DT", 1000, 0, 1000, 1000, int_low, int_high);

    m_hists->BookHist("S2W/Intercept/S2c_Pass", 1000, 0, 6, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/E_Pass", 250, 0, 2500, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/DT_Pass", 1000, 0, 1000, 1000, int_low, int_high);

    m_hists->BookHist("S2W/Intercept/S2c_Fail", 1000, 0, 6, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/E_Fail", 250, 0, 2500, 1000, int_low, int_high);
    m_hists->BookHist("S2W/Intercept/DT_Fail", 1000, 0, 1000, 1000, int_low, int_high);

}

void DH_DataQuality_Cuts::BasicCuts(){
  // // Some basic cuts to conduct on the data coming in
  
  // // Fiducial volume cut
  
  bool FV = sqrt(X*X + Y*Y) < 68.2;

  // // Skin and OD cuts
  //
  // cout << "Skin OD" << endl;
  // These still produce segmentation faults...
  
    bool Skin = true;
    bool OD = true;

  if(SR1){
    Skin = m_cuts->sr1()->SkinVeto(S1ID);
    OD = m_cuts->sr1()->ODVeto(S1ID);
  }
  else{
    Skin = m_cutsDH_DataQuality_Cuts->SkinVeto(S1Start_ns);
    OD = m_cutsDH_DataQuality_Cuts->ODVeto(S1Start_ns);
  }
  // Gas Cut

  bool Gas = TBA < 0.2;

  // Very basic S2c cut
  bool SmallS2 = ss_S2c > 250;

  // // ETrain Cut

  bool ETrain_S1 = true;
  bool ETrain_S2 = true;

  if(SR1){
    ETrain_S1 = m_cuts->sr1()->PassesETrainVeto(S1ID, "S1");
    ETrain_S2 = m_cuts->sr1()->PassesETrainVeto(S2ID, "S2");
  }

  // RFR Cut
  bool RFR = S2TBA > 0 && !((TBA < -0.8) && (S2w < 1000));
  bool AboveAnode = !(TBA > -0.65 && S2TBA > 0.45);

  // S1 MS Cut - remove events with multiple S1s
  bool S1MS = !(*m_evt->SS_isDoubleS1);

  Basic = FV && Skin && OD && Gas && SmallS2 && RFR && S1MS;
  Basic_ETrain = Basic && ETrain_S1 && ETrain_S2;

  // cout << "Done" << endl;

  // Basic Cuts - used to define DQ cuts with minimal removed events
  

  // SR1 Cuts - used in SR1 analysis to remove bad data
  bool Muon = true;
  if(SR1)m_cuts->sr1()->TPCMuonVeto();

  bool Hotspot = true;
  if(SR1)m_cuts->sr1()->HotspotExclusion();

  if(SR1)FV = m_cuts->sr1()->Fiducial();
  bool S2Stinger = true;
  S2Stinger = m_cutsDH_DataQuality_Cuts->s2stinger();

  bool Buffer = true;
  if(SR1) Buffer = m_cuts->sr1()->BufferStartTime() && m_cuts->sr1()->BufferStopTime(S1ID, S2ID);

  bool SR1WS = Muon && ETrain_S1 && ETrain_S2 && Hotspot && FV && S2Stinger && Buffer && OD && Skin;
  

  int nPulses = (*m_evt->nPulses);
  if(nPulses == 2){
    if(SR1WS)m_hists->FillTree("SR1WS");
    m_hists->FillTree("AllSS");
    if(Basic_ETrain)m_hists->FillTree("BasicCuts");
  }
}

void DH_DataQuality_Cuts::Classify()
{
    // Check if the event satisfies cuts for Rn, DD, or Unphy
    // And make some basic XY, R, DT plots for each of these

    // Is the event in the WIMP ROI
    WIMPROI = (ss_S1c > 2.5) && (ss_S1c < 200) && (ss_S2c > 318) && (ss_S2c < 100000);

    // Is the event injected externally
    bool ExternalTrigger = TriggerType == 16;

    // Is the event in non-weird drift times regions?
    bool DT = driftTime_us > 50 && driftTime_us < 855;

    // Specific Cuts:
    // Radon: Flow regions in XY, events at start or end of window
    bool Rn_hotspot = ((X > -40 && X < -23) && ((Y > -30 && Y < -10) || (Y > 12 && Y < 30))) || ((X > -15 && X < 8) && ((Y > -45 && Y < -27) || (Y > -15 && Y < 14) || (Y > 32 && Y < 52) )) || ((X > 28 && X < 42) && ((Y > -25 && Y < -7) || (Y > 7 && Y < 25)));
    bool Rn_S2Trig = S2Start_us > -1000 && S2Start_us < 1000;

    Rn = Basic_ETrain && Rn_S2Trig;
    Rn_Hotspot = Rn && !Rn_hotspot;
    Rn_DT = Rn && DT;
    
    // DD: External to TPC, smaller S2s
    TPCExternal = Basic_ETrain && ExternalTrigger;
    bool DD_S2 = log10(ss_S2c) < 6;
    DD = TPCExternal;

    // Unphy cuts: Just things above drift time
    UDT = driftTime_us > 1000;

}

void DH_DataQuality_Cuts::BasicPlots(string Dir){

    char name[400];
    sprintf(name, "%s/S1S2; S1c [phd]; Log10(S2c [phd])", Dir.c_str());
    m_hists->BookFillHist(name, 1000, 0, 10000, 1000, 0, 7, ss_S1c, log10(ss_S2c));

    sprintf(name, "%s/S1S2ROI; S1c [phd]; Log10(S2c [phd])", Dir.c_str());
    m_hists->BookFillHist(name, 1000, 0, 100, 1000, 0, 7, ss_S1c, log10(ss_S2c));

    sprintf(name, "%s/XY; X [cm]; Y [cm]", Dir.c_str());
    m_hists->BookFillHist(name, 1000, -80, 80, 1000, -80, 80, X, Y);

    sprintf(name, "%s/Volume; Rsq [cm^2]; - Drift Time [us]", Dir.c_str());
    m_hists->BookFillHist(name, 1000, 0, 6400, 1000, -1000, 0, Rsq, -driftTime_us);

    sprintf(name, "%s/TBAs; S1 TBA; S2 TBA", Dir.c_str());
    m_hists->BookFillHist(name, 1000, -1, 1, 1000, -1, 1, TBA, S2TBA);
}

void DH_DataQuality_Cuts::BookBasicPlots(string Dir){
    char name[400];
    sprintf(name, "%s/S1S2; S1c [phd]; Log10(S2c [phd])", Dir.c_str());
    m_hists->BookHist(name, 1000, 0, 10000, 1000, 0, 7);

    sprintf(name, "%s/S1S2ROI; S1c [phd]; Log10(S2c [phd])", Dir.c_str());
    m_hists->BookHist(name, 1000, 0, 100, 1000, 0, 7);

    sprintf(name, "%s/XY; X [cm]; Y [cm]", Dir.c_str());
    m_hists->BookHist(name, 1000, -80, 80, 1000, -80, 80);

    sprintf(name, "%s/Volume; Rsq [cm^2]; - Drift Time [us]", Dir.c_str());
    m_hists->BookHist(name, 1000, 0, 6400, 1000, -1000, 0);

    sprintf(name, "%s/TBAs; S1 TBA; S2 TBA", Dir.c_str());
    m_hists->BookHist(name, 1000, -1, 1, 1000, -1, 1);
}

// TBA DQ Plots Start Here

void DH_DataQuality_Cuts::TBADT()
{    

    // Plots of S2 width vs drift time for all data types
    m_hists->BookFillHist("TBA/Plots/TBADT_All", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
    m_hists->BookFillHist("TBA/Plots/TBADTS2_All", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);

    if(WIMPROI){
        m_hists->BookFillHist("TBA/Plots/TBADT_All_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_All_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
    }

    if(Basic){
        m_hists->BookFillHist("TBA/Plots/TBADT_Basic", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_Basic", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
    }

    if(Basic_ETrain){
        m_hists->BookFillHist("TBA/Plots/TBADT_Basic_ETrain", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_Basic_ETrain", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
        
        if(WIMPROI){
            m_hists->BookFillHist("TBA/Plots/TBADT_Basic_ETrain_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
            m_hists->BookFillHist("TBA/Plots/TBADTS2_Basic_ETrain_ROI", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
        }
    }

    if(Rn){
        m_hists->BookFillHist("TBA/Plots/TBADT_Rn", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_Rn", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
    }
    if(Rn_Hotspot){
        m_hists->BookFillHist("TBA/Plots/TBADT_Rn_Hotspot", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_Rn_Hotspot", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
    }
    if(DD){
        m_hists->BookFillHist("TBA/Plots/TBADT_DD", 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_DD", 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);
    }
    if(UDT){
        m_hists->BookFillHist("TBA/Plots/TBADT_All", 1000, 1000, 2000, 1000, -1, 1, driftTime_us, TBA);
        m_hists->BookFillHist("TBA/Plots/TBADTS2_All", 1000, 1000, 2000, 1000, -1, 1, driftTime_us, S2TBA);
    }

    // Ideal TBA-DT mean relationship 
    // Using Kr data
    double A = -0.0671659;
    double B = -0.000386112;
    double C = -9.78792E-7;
    double D = 1.17684E-9;
    double E = -3.63852E-13;

    // Non-linear tails
    if(Basic_ETrain){
        double Tail = TBA - A - B*driftTime_us;

        m_hists->BookFillHist("TBA/Tail/DT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, Tail);
        m_hists->BookFillHist("TBA/Tail/S1c", 1000, 0, 10000, 1000, -1, 1, ss_S1c, Tail);
        m_hists->BookFillHist("TBA/Tail/S1c_ROI", 1000, 0, 100, 1000, -1, 1, ss_S1c, Tail);

        double Residual = TBA - A - B*driftTime_us
            - C*driftTime_us*driftTime_us
            - D*driftTime_us*driftTime_us*driftTime_us
            - E*driftTime_us*driftTime_us*driftTime_us*driftTime_us;
        
        m_hists->BookFillHist("TBA/Residual/DT", 1000, 0, 1000, 1000, -1, 1, driftTime_us, Residual);
        m_hists->BookFillHist("TBA/Residual/S1c", 1000, 0, 10000, 1000, -1, 1, ss_S1c, Residual);
        m_hists->BookFillHist("TBA/Residual/S1c_ROI", 1000, 0, 100, 1000, -1, 1, ss_S1c, Residual);

        double minKr[5] = {-0.304516, -0.000772908, 6.40951e-08, -2.00941e-10, 3.00537e-13};
        double maxKr[5] = {0.170184, -0.000386112, -2.02168e-06, 2.55463e-09, -1.02824e-12};

        double minTBA = minKr[0]
            + minKr[1]*driftTime_us 
            + minKr[2]*driftTime_us*driftTime_us 
            + minKr[3]*driftTime_us*driftTime_us*driftTime_us 
            + minKr[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

        double maxTBA = maxKr[0]
            + maxKr[1]*driftTime_us 
            + maxKr[2]*driftTime_us*driftTime_us 
            + maxKr[3]*driftTime_us*driftTime_us*driftTime_us 
            + maxKr[4]*driftTime_us*driftTime_us*driftTime_us*driftTime_us;

        if(TBA > minTBA && TBA < maxTBA){
            m_hists->BookFillHist("TBA/Residual/DT_Pass", 1000, 0, 1000, 1000, -1, 1, driftTime_us, Residual);
            m_hists->BookFillHist("TBA/Residual/S1c_Pass", 1000, 0, 10000, 1000, -1, 1, ss_S1c, Residual);
            m_hists->BookFillHist("TBA/Residual/S1c_ROI_Pass", 1000, 0, 100, 1000, -1, 1, ss_S1c, Residual);
        }
        else{
            m_hists->BookFillHist("TBA/Residual/DT_Fail", 1000, 0, 1000, 1000, -1, 1, driftTime_us, Residual);
            m_hists->BookFillHist("TBA/Residual/S1c_Fail", 1000, 0, 10000, 1000, -1, 1, ss_S1c, Residual);
            m_hists->BookFillHist("TBA/Residual/S1c_ROI_Fail", 1000, 0, 100, 1000, -1, 1, ss_S1c, Residual);
        }
    }

    // Handscan regions:
    
    // Good Events
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1400 && S2w < 1600){
    //     m_hists->BookFillHist("TBA/Handscans/Good/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("TBA/Handscans/Good");
    //     S2W_Good->SparseLogData(m_event);
    // }

    // Bad Events
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1600 && S2w < 3400){
    //     m_hists->BookFillHist("TBA/Handscans/Bad/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("TBA/Handscans/Bad");
    //     S2W_Bad->SparseLogData(m_event);
    // }

}

void DH_DataQuality_Cuts::BookTBADT(){

    // Plots of S2 width vs drift time for all data types
    m_hists->BookHist("TBA/Plots/TBADT_All", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_All; Drift Time [us]; S2 TBA", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_All_ROI", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_All_ROI", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_Basic", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_Basic", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_Basic_ETrain", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_Basic_ETrain", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_Basic_ETrain_ROI", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_Basic_ETrain_ROI", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_Rn", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_Rn", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_Rn_Hotspot", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_Rn_Hotspot", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_DD", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_DD", 1000, 0, 1000, 1000, -1, 1);

    m_hists->BookHist("TBA/Plots/TBADT_All", 1000, 1000, 2000, 1000, -1, 1);
    m_hists->BookHist("TBA/Plots/TBADTS2_All", 1000, 1000, 2000, 1000, -1, 1);

    m_hists->BookHist("TBA/Tail/DT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Tail/S1c", 1000, 0, 10000, 1000, -1, 1);
    m_hists->BookHist("TBA/Tail/S1c_ROI", 1000, 0, 100, 1000, -1, 1);
       
    m_hists->BookHist("TBA/Residual/DT", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c", 1000, 0, 10000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c_ROI", 1000, 0, 100, 1000, -1, 1);

    m_hists->BookHist("TBA/Residual/DT_Pass", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c_Pass", 1000, 0, 10000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c_ROI_Pass", 1000, 0, 100, 1000, -1, 1);

    m_hists->BookHist("TBA/Residual/DT_Fail", 1000, 0, 1000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c_Fail", 1000, 0, 10000, 1000, -1, 1);
    m_hists->BookHist("TBA/Residual/S1c_ROI_Fail", 1000, 0, 100, 1000, -1, 1);

    // Handscan regions:
    
    // Good Events
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1400 && S2w < 1600){
    //     m_hists->BookFillHist("TBA/Handscans/Good/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("TBA/Handscans/Good");
    //     S2W_Good->SparseLogData(m_event);
    // }

    // Bad Events
    // if(driftTime_us > 540 && driftTime_us < 550 && S2w > 1600 && S2w < 3400){
    //     m_hists->BookFillHist("TBA/Handscans/Bad/S2WDT; Drift Time [us]; S2 AFT75 - AFT25 [ns]", 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);
    //     BasicPlots("TBA/Handscans/Bad");
    //     S2W_Bad->SparseLogData(m_event);
    // }

}

void DH_DataQuality_Cuts::TBA_Slices(){
    // Take slices of S2 width for - S1 area, S2 area, Energy (rough)

    // Check S2 area against slices
    double SE = 60;

    int S2 = 0;

    for(int i = 0; i < 12; i++){
        double Low = S2_low[i]*SE;
        double High = S2_high[i]*SE;
        if(ss_S2c > Low && ss_S2c < High)break;
        S2 += 1;
    }

    // Check S1 area against slices

    int S1 = 0;

    for(int i = 0; i < 19; i++){
        double Low = S1_low[i];
        double High = S1_high[i];
        if(ss_S1c > Low && ss_S1c < High)break;
        S1 += 1;
    }

    // Generate preliminary energy estimate, check against slices

    int N_E = (int)(E_Max/E_Step);

    int E_low = 0;
    int E_high = 0;
    int E = 0;
    for(int i = 0; i < N_E; i++){
        E_low = i*E_Step;
        E_high = E_low + E_Step;
        if(Energy > E_low && Energy < E_high)break;
        E += 1;
    }

    char name[400];

    // Plot S2 Slices
    if(S2 < 11){
        // sprintf(name, "TBA/S2Slices/All/%i_%i/S2WDT", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "TBA/S2Slices/All/%i_%i/S2WDTSQ", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "TBA/S2Slices/All/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
        // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/TBADT", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);

            sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/TBADTS2", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);

            sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
            m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "TBA/S2Slices/Rn/%i_%i/S2WDT", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "TBA/S2Slices/Rn/%i_%i/S2WDTSQ", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "TBA/S2Slices/Rn/%i_%i/S1S2", S2_low[S2], S2_high[S2]);
            // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

    // Plot S1 Slices
    if(S1 < 19){

        int Low = S1_low[S1];
        int High = S1_high[S1];


        // sprintf(name, "TBA/S1Slices/All/%i_%i/S2WDT", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "TBA/S1Slices/All/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "TBA/S1Slices/All/%i_%i/S1S2", Low, High);
        // m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/TBADT", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);

            sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/TBADTS2", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);

            sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/S1S2", Low, High);
            m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "TBA/S1Slices/Rn/%i_%i/S2WDT", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "TBA/S1Slices/Rn/%i_%i/S2WDTSQ", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "TBA/S1Slices/Rn/%i_%i/S1S2", Low, High);
            // m_hists->BookFillHist(name, 1000, 0, 300, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

    // Plot E Slices
    if(E < N_E){
        // sprintf(name, "TBA/ESlices/All/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

        // sprintf(name, "TBA/ESlices/All/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

        // sprintf(name, "TBA/ESlices/All/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));

        if(Basic_ETrain){
            sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/TBADT", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, TBA);

            sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/TBADTS2", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 1000, 1000, -1, 1, driftTime_us, S2TBA);

            sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/S1S2", E_low, E_high);
            m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }

        if(Rn){
            // sprintf(name, "TBA/ESlices/Rn/%i_%i/S2WDT", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max, driftTime_us, S2w);

            // sprintf(name, "TBA/ESlices/Rn/%i_%i/S2WDTSQ", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max, driftTime_us, S2w*S2w);

            // sprintf(name, "TBA/ESlices/Rn/%i_%i/S1S2", E_low, E_high);
            // m_hists->BookFillHist(name, 1000, 0, 10000, 400, 0, 7, ss_S1c, TMath::Log10(ss_S2c));
        }
    }

}

void DH_DataQuality_Cuts::BookTBA_Slices(){
    // Book slices of S2 width for - S1 area, S2 area, Energy (rough)

    char name[400];

    // Check S2 area against slices
    double SE = 60;

    int S2 = 0;

    for(int i = 0; i < sizeof(S2_low); i++){

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDT", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S2WDTSQ", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S2Slices/All/%i_%i/S1S2", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/TBADT", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/TBADTS2", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/S2Slices/Basic_ETrain/%i_%i/S1S2", S2_low[i], S2_high[i]);
        m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDT", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S2WDTSQ", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S2Slices/Rn/%i_%i/S1S2", S2_low[i], S2_high[i]);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);
        
    }

    // Check S1 area against slices

    for(int i = 0; i < 19; i++){
        
        int Low = S1_low[i];
        int High = S1_high[i];

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDT", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S1Slices/All/%i_%i/S1S2", Low, High);
        // m_hists->BookHist(name, 1000, 0, 300, 400, 0, S2W_Max);

        sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/TBADT", Low, High);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/TBADTS2", Low, High);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/S1Slices/Basic_ETrain/%i_%i/S1S2", Low, High);
        m_hists->BookHist(name, 1000, 0, 300, 400, 0, 7);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDT", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S2WDTSQ", Low, High);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/S1Slices/Rn/%i_%i/S1S2", Low, High);
        // m_hists->BookHist(name, 1000, 0, 300, 400, 0, 7);

    }

    // Generate preliminary energy estimate, check against slices

    int N_E = (int)(E_Max/E_Step);

    for(int i = 0; i < N_E; i++){
        int E_low = i*E_Step;
        int E_high = E_low + E_Step;

        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/ESlices/All/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/TBADT", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/TBADTS2", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 1000, 1000, -1, 1);

        sprintf(name, "TBA/ESlices/Basic_ETrain/%i_%i/S1S2", E_low, E_high);
        m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDT", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S2WDTSQ", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 1000, 400, 0, S2W_Max*S2W_Max);

        // sprintf(name, "S2W/ESlices/Rn/%i_%i/S1S2", E_low, E_high);
        // m_hists->BookHist(name, 1000, 0, 10000, 400, 0, 7);
    }

}
