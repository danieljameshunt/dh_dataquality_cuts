#include "DH_DataQuality_CutsEvent.h"

DH_DataQuality_CutsEvent::DH_DataQuality_CutsEvent(EventBase* base): 
    
    tpcHGPulseArea(base->m_reader, "pulsesTPCHG.pulseArea_phd"),
    areaFractionTime50_ns(base->m_reader, "pulsesTPCHG.areaFractionTime50_ns"),
    nPulses(base->m_reader, "pulsesTPCHG.nPulses"),

    SS_nSingleScatters(base->m_reader, "ss.nSingleScatters"),
    SS_s1PulseID(base->m_reader, "ss.s1PulseID"),
    SS_s1Area(base->m_reader, "ss.s1Area_phd"),
    SS_s2Area(base->m_reader, "ss.s2Area_phd"),
    SS_correctedS1Area(base->m_reader, "ss.correctedS1Area_phd"),
    SS_correctedS2Area(base->m_reader, "ss.correctedS2Area_phd"),
    SS_driftTime_ns(base->m_reader, "ss.driftTime_ns"),
    SS_X_cm(base->m_reader, "ss.x_cm"),
    SS_Y_cm(base->m_reader, "ss.y_cm"),
    SS_s2PulseID(base->m_reader, "ss.s2PulseID"),
    SS_isDoubleS1(base->m_reader, "ss.isDoubleS1"),

    Event_Trigger(base->m_reader, "eventHeader.triggerType"),

    TPC_Classification(base->m_reader, "pulsesTPC.classification"),

    TPC_nPulses(base->m_reader, "pulsesTPC.nPulses"),
    TPC_pulseArea(base->m_reader, "pulsesTPC.pulseArea_phd"),
    TPC_pulseTBA(base->m_reader, "pulsesTPC.topBottomAsymmetry"),
    TPC_pulseStartTime_ns(base->m_reader, "pulsesTPC.pulseStartTime_ns"),
    TPC_pulseEndTime_ns(base->m_reader, "pulsesTPC.pulseEndTime_ns"),
    TPC_pulseS2XPosition_cm(base->m_reader, "pulsesTPC.s2Xposition_cm"),
    TPC_pulseS2YPosition_cm(base->m_reader, "pulsesTPC.s2Yposition_cm"),
    TPC_s1PulseIDs(base->m_reader, "pulsesTPC.s1PulseIDs"),
    TPC_s2PulseIDs(base->m_reader, "pulsesTPC.s2PulseIDs"),
    TPC_s2PulseIDsByArea(base->m_reader, "pulsesTPC.s2PulseIDsByArea"),
    TPC_SPE(base->m_reader, "pulsesTPC.singlePEprobability"),
    TPC_Amp(base->m_reader, "pulsesTPC.peakAmp"),

    TPCHG_AFT10(base->m_reader, "pulsesTPCHG.areaFractionTime10_ns"),  
    TPCHG_AFT25(base->m_reader, "pulsesTPCHG.areaFractionTime25_ns"),
    TPCHG_AFT75(base->m_reader, "pulsesTPCHG.areaFractionTime75_ns"),
    TPCHG_AFT5(base->m_reader, "pulsesTPCHG.areaFractionTime5_ns"),
    TPCHG_AFT50(base->m_reader, "pulsesTPCHG.areaFractionTime50_ns"),
    TPCHG_AFT90(base->m_reader, "pulsesTPCHG.areaFractionTime90_ns"),
    TPCHG_AFT95(base->m_reader, "pulsesTPCHG.areaFractionTime95_ns"),

    TPCHG_chPulseArea(base->m_reader, "pulsesTPCHG.chPulseArea_phd"),

    OD_nHGPulses(base->m_reader, "pulsesODHG.nPulses"),
    OD_HGPulseArea(base->m_reader, "pulsesODHG.pulseArea_phd"),
    OD_HGPulseStartTime_ns(base->m_reader, "pulsesODHG.pulseStartTime_ns"),
    Skin_nPulses(base->m_reader, "pulsesSkin.nPulses"),
    Skin_PulseArea(base->m_reader, "pulsesSkin.pulseArea_phd"),
    Skin_PulseStartTime_ns(base->m_reader, "pulsesSkin.pulseStartTime_ns")

{
}

DH_DataQuality_CutsEvent::~DH_DataQuality_CutsEvent()
{
}
